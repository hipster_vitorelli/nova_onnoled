import Swal from 'sweetalert2';

export function Formulario(){
    if($("body.faleconosco").length > 0){
	
        $("input.telefone").mask('(00) 0000-0000');
        $("input.celular").mask('(00) 00000-0000');
        $(".botao-envia-form .botao").click(function(){
            var cliente = {
                "nome": $("input.nome").val(),
                "sobrenome": $("input.sobrenome").val(),
                "telefone": $("input.telefone").val(),
                "celular": $("input.celular").val(),
                "email": $("input.email").val(),
                "solicitacao": $("select#solicitacao option:selected").val(),
                "mensagem": $("textarea.mensagem").val(),
            };
            $.ajax({
                type: "POST",
                url: window.location.origin + "/api/dataentities/FC/documents",
                processData: false,
                data: JSON.stringify(cliente),
                cache: false,
                async: true,
                crossDomain: true,
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/vnd.vtex.ds.v10+json"
                },
                success: function(result) {
                    Swal.fire({
                        title: 'Seu contato é muito importante!',
                        text: 'Retornaremos o mais breve possível.',
                        animation: false
                    });
                    $("input").empty().val("");
                    $("textarea").empty().val("");
                },
                error: function(result) {
                    Swal.fire({
                        title: 'Ops, algo deu errado',
                        text: result.responseText.split(":")[1].split("}")[0],
                        animation: false,
                        type: 'error'
                    });
                }
            });
        }); 
    }
}
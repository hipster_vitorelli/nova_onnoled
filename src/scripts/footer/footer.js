import Swal from 'sweetalert2';

export function Newsletter(){

    $(".enviar-news").click(function(){
        var cliente = {
            "nome": $(".newsletter-site input.nome").val(), 
            "email": $('.newsletter-site input.email').val()
        };
        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/dataentities/NL/documents",
            processData: false,
            data: JSON.stringify(cliente),
            cache: false,
            async: true,
            crossDomain: true,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/vnd.vtex.ds.v10+json"
            },
            success: function(result) {
                Swal.fire({
                    title: 'Obrigado por se cadastrar!',
                    text: 'Em breve você ira receber novidades, promoções e muito mais',
                    animation: false
                });
                $("input").empty().val("");
                $("textarea").empty().val("");
            },
            error: function(result) {
                Swal.fire({
                    title: 'Ops, algo deu errado',
                    text: result.responseText.split(":")[1].split("}")[0],
                    animation: false,
                    type: 'error'
                });
            }
        });
    });
	
}
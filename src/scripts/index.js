import '../styles/index.scss';
import $ from 'jquery';
import "jquery-mask-plugin";
import "slick-carousel";
 
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import {favicon} from "../scripts/favicon/favicon";
import {Banner} from "../scripts/banner/banner";
import {Carrossel} from "../scripts/carrossel/carrossel";
import { Search } from './busca/autocomplete';
import { Login } from './login/login';
import { Minicart } from './minicart/minicart';
import Mobile from './mobile/mobile';
import { Newsletter } from './footer/footer';

Mobile();
Newsletter();
//favicon
favicon();
$(".search-single-navigator h3").text("Modelos").show();

Carrossel(".banners-destaques", ".box-banner", 3,3,2,2,2,1);
Carrossel(".colecoes .colecao .prateleira ul", "li", 4,3,3,2,2,1);
Carrossel("ul.thumbs", "li", 6,5,4,3,2,2);

Banner(".banners");

$("body").click(function(event){
    let evento = event.target.nodeName;
    if(evento === 'IMG' || evento === 'INPUT'){
        let nomeClick = event.target.className;
        if(nomeClick == 'hover-busca' || nomeClick == 'busca-frame'){
            $(".busca").addClass('active');
        }else{
            $(".busca").removeClass('active');
        }
    }else{
        $(".busca").removeClass('active');
    }
});

Search();
Login();
Minicart();

$(".icon-carrinho").click(function(){
    $(".carrinho-interno").slideDown();
    $(".qtd-cart-topo").fadeToggle();
});

$(".imagem-acesso").click(function(){
    $(".submenucliente").slideToggle();
});

if($('body.categoria').length > 0 || $('body.departamento').length > 0 || $('body.busca').length > 0 || $('body.busca-vazia').length > 0 || $('body.institucional').length > 0){
    $('a[title="varixx"], li[typeof="v:Breadcrumb"] a').text("Home");
}

if($('body.produto').length > 0){
    $('li[typeof="v:Breadcrumb"]:eq(0) a').text("Home");
    var nome_bread = $(".fn.productName").text();
    $("section.master.bread-Crumb .bread-crumb ul li").removeClass("last");
    $("section.master.bread-Crumb .bread-crumb ul").append('<li class="last">'+nome_bread+'</li>');

    setInterval(function(){
        var valorNovo = $(".descricao-preco .valor-por").text().replace("Por: ", "");
        $(".descricao-preco .valor-por").text(valorNovo);
    },10);
}

if($('body.busca').length > 0 || $('body.busca-vazia').length > 0){
    var queryBusca = vtxctx.searchTerm;
    $('section.master.sessao-titulo-categorias h2.titulo-sessao').append(' : "'+queryBusca+'"');
}

if($('body.institucional').length > 0){
    var queryBusca = vtxctx.searchTerm;
    $('section.master.sessao-titulo-categorias .content .titulo').append('<h2 class="titulo-sessao">'+queryBusca+'</h2>');
}

if($('body.categoria').length > 0 || $('body.departamento').length > 0 || $('body.busca').length > 0){
    var encontrados = $(".resultado-busca-numero:eq(0) span.value").text();
    $(".box.quantidade-encontrada").empty().append('<div><strong>'+encontrados+'</strong> produtos encontrados</div>');
    var orderm = $("select#O:eq(0)");
    $(".box.ordenacao").empty().append(orderm);

    setInterval(function(){
        $(".helperComplement").remove();
    },500);

    $("ul.Cor").find("li").each(function(){
        var cor = $(this).find("a").attr("title");
        $(this).find('a').text(cor);
        $(this).find('a').attr('ref', cor);
    });
    $("li.filtro-ativo").each(function(){
        var th = $(this).parent('ul').prev().html();
        $(this).addClass(th);

        var cor = $(this).text();
        $(this).empty();
        $(this).append('<a ref="'+cor+'">'+cor+'</a>');
    });
}

if($('body.produto').length > 0){

    var idP = $("input#___rc-p-id").attr('value');
    var settings = {
      "url": window.location.origin + "/api/catalog_system/pvt/products/"+idP+"/specification",
      "method": "GET",
      "timeout": 0,
      "headers": {
        "X-VTEX-API-AppKey": "vtexappkey-varixx-ANZFMX",
        "X-VTEX-API-AppToken": "URUIOTKGWAUYDGNFCBUTSTSRBHDJIGBBBRDRHBIDXSGNWTGEDMWPLXBAZCHGQBXQZTWMDVXBTRWANEVIBYFWJLPDQCSFUBNVUQNCWUBHOUWUBANUHYFRVBGOESVADSHR"
      },
    };
    
    $.ajax(settings).done(function (response) {
    let banner = response.filter(req => req.Id == 36).map(req => String(req.Value[0]));
            if(banner.length > 0){
                let bannerFim = String(banner);
                $(".produto-descricao-completa").append('<img style="max-width: 100%; width: 100%;" src="/arquivos/'+bannerFim+'" />');
            }
    
            let banner2 = response.filter(req => req.Id == 47).map(req => String(req.Value[0]));
            if(banner2.length > 0){
                let bannerFim2 = String(banner2);
                $(".produto-descricao-completa").append('<a class="img-com-link-retorno" href=""><img style="max-width: 100%; width: 100%;" src="/arquivos/'+bannerFim2+'" /></a>');
            }

            let link_retorno = response.filter(req => req.Id == 48).map(req => String(req.Value[0]));
            if(link_retorno.length > 0){
                let link_retorno_fim = String(link_retorno);
                $(".img-com-link-retorno").attr("href", link_retorno_fim);
            }
    });
}
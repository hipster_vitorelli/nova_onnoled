const axios = require('axios');
import {TemplateUserLogado, TemplateUserNaoLogado} from "../templates/acesso";
 
export function Login(){
    axios.get(window.location.origin + '/no-cache/profileSystem/getProfile').then(function (response) {
        let objeto = response.data;
        let email = objeto.Email;
        if(objeto.IsUserDefined == false){

            const listComponent = () => {
                return (
                    TemplateUserNaoLogado()
                );
            };
            const el = document.querySelector('.menu-exibi-acesso');
            el.innerHTML = listComponent();

        }else{

            $('.submenucliente .conteudo .nomecliente').html(`Olá, <strong>${email}</strong>`);
            const listComponent = () => {
                return (
                    TemplateUserLogado()
                );
            };
            const el = document.querySelector('.menu-exibi-acesso');
            el.innerHTML = listComponent();

        }
    }).catch(function (error) {
        console.log(error);
    });
}



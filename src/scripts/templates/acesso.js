export const TemplateUserLogado = () => {
    return `<nav class="acesso-cliente"><a href="/logout">Sair</a><a href="/account">Minha conta</a><a href="/account/orders">Meus pedidos</a></nav>`;
};
export const TemplateUserNaoLogado = () => {
    return `<nav class="acesso-cliente"><a href="/login">Login / Cadastre-se</a></nav>`;
};
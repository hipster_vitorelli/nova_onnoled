export const TemplateCarrinhoSemPrecoDe = (index, idsku, foto, url, nome, precoPor, quantidade) => {
    return `<div class="orderform-cart" index="${index}" idsku="${idsku}">
                <a>
                    <div class="produto-indice">
                        <h3>Produto</h3>  
                        <div class="titulo">
                            <div class="box foto">
                                <img src="${foto}" alt="" />
                            </div>
                            <div class="box descricao">
                                <div class="nome">'${nome}'</div>
                                <div class="variacao"></div>
                            </div>
                        </div>
                    </div>
                    <div class="precos">
                        <h3>Preço</h3>
                        <div class="precos">
                            <div class="precoPor">'${precoPor}'</div>
                        </div>
                    </div>
                    <div class="quantidades">
                        <h3>QTD.</h3>
                        <div class="quantidade"><strong>${quantidade}</strong></div>
                        </div>
                    <div class="excluir-produtos">
                        <h3>Excluir</h3>
                        <span class="lnr lnr-cross"></span>
                    </div>
                </a>
            </div>`;
};
export const TemplateCarrinhoComPrecoDe = (index, idsku, foto, url, nome, precoDe ,precoPor, quantidade) => {
    return `<div class="orderform-cart" index="${index}" idsku="${idsku}">
                <a>
                    <div class="produto-indice">
                        <h3>Produto</h3>  
                        <div class="titulo">
                            <div class="box foto">
                                <img src="${foto}" alt="" />
                            </div>
                            <div class="box descricao">
                                <div class="nome">'${nome}'</div>
                                <div class="variacao"></div>
                            </div>
                        </div>
                    </div>
                    <div class="precos">
                        <h3>Preço</h3>
                        <div class="precos">
                            <div class="precoDe">'${precoDe}'</div>
                            <div class="precoPor">'${precoPor}'</div>
                        </div>
                    </div>
                    <div class="quantidades">
                        <h3>QTD.</h3>
                        <div class="quantidade"><strong>${quantidade}</strong></div>
                        </div>
                    <div class="excluir-produtos">
                        <h3>Excluir</h3>
                        <span class="lnr lnr-cross"></span>
                    </div>
                </a>
            </div>`;
};
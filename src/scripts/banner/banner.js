import $ from 'jquery';
export function Banner(classe){
    $(classe).slick({
        dots: true,
        infinite: false,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 5000,
        nextArrow: '<span class="lnr lnr-chevron-right"></span>',
        prevArrow: '<span class="lnr lnr-chevron-left"></span>',
        arrows: false
    });
}
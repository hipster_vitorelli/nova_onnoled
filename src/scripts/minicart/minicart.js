import { TemplateCarrinhoSemPrecoDe, TemplateCarrinhoComPrecoDe  } from "../templates/minicart";

export function Minicart(){
	vtexjs.checkout.getOrderForm().done(function(orderForm) {
		$("li.icon-carrinho .qtd-cart-topo").empty();
		if(orderForm.items.length < 10){
			$("li.icon-carrinho .qtd-cart-topo").append('<p class="qtd-cart">0'+orderForm.items.length+'</p>');
		}else{
			$("li.icon-carrinho .qtd-cart-topo").append('<p class="qtd-cart">'+orderForm.items.length+'</p>');
		}
		
		var itemscart = orderForm.items;
		var preco_cart = orderForm.value / 100;
		preco_cart = preco_cart.toLocaleString('pt-br', {
			style: 'currency',
			currency: 'BRL'
		});
		$(".carrinho-interno .conteudo .infocarrinho h4").text(preco_cart);
		if(itemscart.length == 0){
			$(".carrinho-vazio").show().empty().append("<h3>Seu carrinho está vazio!</h3><a href='/'>Adicionar produtos</a>");
			$(".infocarrinho").hide();
			$(".carrinho-cheio").hide().empty();
		}else{
			$(".carrinho-vazio").hide();
			$(".infocarrinho").show();
			$(".carrinho-cheio").empty().append("<div class='produtos-encontrados'></div>");
			var index = 0;
			for(var i = 0; i < itemscart.length; i++){
				console.log(itemscart[i]);
				var idsku = itemscart[i].id;
				var quantidade = itemscart[i].quantity;
				var nomeProduto = itemscart[i].skuName;
				var precoDe = itemscart[i].listPrice / 100;
				precoDe = precoDe.toLocaleString('pt-br', {
					style: 'currency',
					currency: 'BRL'
				});
				var precoPor = itemscart[i].price / 100;
				precoPor = precoPor.toLocaleString('pt-br', {
					style: 'currency',
					currency: 'BRL'
				});
				var idsku = itemscart[i].id;
				var foto = itemscart[i].imageUrl.replace("-110-110", "-80-80");
				var url = itemscart[i].detailUrl;
				if(precoDe === precoPor){
					const listComponent = () => {
                        return (
                            TemplateCarrinhoSemPrecoDe(index, idsku, foto, url, nomeProduto, precoPor, quantidade)
                        );
                    };
                    $(".produtos-encontrados").append(listComponent());
                    index = index + 1;
				}else{
					const listComponent = () => {
                        return (
                            TemplateCarrinhoComPrecoDe(index, idsku, foto, url, nomeProduto, precoDe ,precoPor, quantidade)
                        );
                    };
                    $(".produtos-encontrados").append(listComponent());
                    index = index + 1;
				}
			}
		}
		
		$(".fechar-carrinho span, .infocarrinho .botoes .continuar").click(function(){
			$(".carrinho-interno").fadeOut(350);
        });
        
		$(".excluir-produtos span").click(function(){
			var index = $(this).parents(".orderform-cart").attr('index');
			var quantidade = parseInt($(this).parents(".orderform-cart").find('.quantidade strong').text());
			var itemindex = $(this).parents(".orderform-cart").attr("index");
			vtexjs.checkout.getOrderForm().then(function(orderForm) {
				var itemIndex = itemindex;
				var item = orderForm.items[itemIndex];
				var itemsToRemove = [
					{
						"index": index,
						"quantity": quantidade,
					}
				];
				return vtexjs.checkout.removeItems(itemsToRemove);
			}).done(function(orderForm) {
				Minicart();
			});
		});
	
	});
}
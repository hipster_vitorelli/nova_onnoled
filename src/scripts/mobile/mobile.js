export default function Mobile(){
    let largura = $(window).width();
    if(largura < 710){
        $("header.header-site .acesso-loja ul").prepend('<li class="menu-hamburguer"><i class="fas fa-bars"></i></li>');

        setInterval(function(){
            var altura = $(window).height();
            $(".resultado").css('max-height', altura+"px");
            $(".menu").css('max-height', altura+"px");
            $("header.header-site .menu > ul").css('max-height', altura+"px");
        });

        $("li.menu-hamburguer").click(function(){
            $(".menu").show().css("display", "flex !important");
        });

        $("header.header-site .menu > ul").find("> li").each(function(){
            if($(this).find(".submenu").length > 0){
                $(this).find("> a").removeAttr("href");
            }
        });
        $("header.header-site .menu > ul").find("> li a").click(function(){
            $(this).siblings(".submenu").slideToggle().parents('li').siblings('li').find(".submenu").slideUp();
        });
        $(".submenu.submenu-modelos .controle > .box > ul").find("li").click(function(){
            var urlpath = $(this).find("a").attr('href');
            window.location.href = window.location.origin + urlpath;
        });
    }
}
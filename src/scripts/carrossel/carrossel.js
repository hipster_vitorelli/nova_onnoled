import $ from 'jquery';
import "slick-carousel";
export function Carrossel(classe, filho, show1, show2, show3, show4, show5, show6){
    var $item = $(''+classe+'');
    $item.find('.helperComplement').remove();
    if ($item.find(''+filho+'').length > 0) {
        $item.slick({
            infinite: false,
            speed: 600,
            slidesToShow: show1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            dots: false,
            nextArrow: '<span class="lnr lnr-chevron-right"></span>',
            prevArrow: '<span class="lnr lnr-chevron-left"></span>',
            responsive: [{
                    breakpoint: 1030,
                    settings: {
                        slidesToShow: show2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 879,
                    settings: {
                        slidesToShow: show3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 790,
                    settings: {
                        slidesToShow: show4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 750,
                    settings: {
                        slidesToShow: show5,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: show6,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    }
}
setTimeout(function(){
    setInterval(function(){
        var quantidadeAtivo = $(".destaques .slick-slide.slick-active").length;
        $(".destaques .slick-slide.slick-active:eq("+Number(quantidadeAtivo - 1)+")").css("margin", "0px");
        $(".destaques .slick-slide.slick-active:not(.slick-active:eq("+Number(quantidadeAtivo - 1)+"))").css("margin", "0px 30px");
        $(".destaques .slick-slide.slick-active:eq(0)").css("margin", "0px 0px");
    },100);
},1000);
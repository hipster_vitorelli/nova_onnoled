export function Autocomplete(search){
    var settings = {
        "url": window.location.origin+"/buscaautocomplete/?productNameContains="+search,
        "method": "GET",
        "timeout": 0,
    };
    $.ajax(settings).done(function (response) {
        $(".busca .resultado").empty();
        var retorno = response.itemsReturned;
        for(var i = 0; i < retorno.length; i++){
            var item = retorno[i];

            if(item.items.length > 0){
                var urlimg = item.items[0].imageUrl;
                urlimg = urlimg.replace("-25-25", "-90-90");
                var prod = `<div class="produto-search" id="${item.items[0].itemId}">
                                <a href="${item.href}">
                                    <div class="img-foto">
                                        <img src="${urlimg}" alt="${item.items[0].nameComplete}" />
                                    </div>
                                    <div class="nome-prod">
                                        <h2>${item.items[0].nameComplete}</h2>
                                    </div>
                                </a>
                            </div>`;
                
                $(".busca .resultado").append(prod);
            }

        }
    });
}

export function Search(){
    $('.busca-frame').keyup(function(){
        var search = $(this).val();
        if($('.busca-frame').val().length >=3){
            console.log("valor maior de 3");
            Autocomplete(search);
            SearchEnter(search);
        }else{
            console.log("valor menor de 3");
            $(".busca .resultado").empty();
        }
    });
    
}

function SearchEnter(search){
    document.addEventListener('keypress', function(e){
        if(e.which == 13){
           window.location.href = ""+window.location.origin+"/"+search;
        }
     }, false);
     $(".img.hover-busca").click(function(){
        var search = $('.busca-frame').val();
        window.location.href = ""+window.location.origin+"/"+search;
     });
}